#include <iostream>

/////////////////////////////////////////////////////////////////////////////////////////
//// VikBo - pattern Abstract Factory
/////////////////////////////////////////////////////////////////////////////////////////


// Interface IStatistics
class IStatistics
{
    virtual void PrintStatistics(std::string AppendString) = 0;
};
/////////////////////////////////////////////////////////////////////////////////////////
//// MESH
/////////////////////////////////////////////////////////////////////////////////////////

// Abstract class Mesh
class Mesh: public IStatistics
{
protected:
    int Height{0};
    int Weight{0};
public:
    Mesh() = default;
    virtual ~Mesh() = default;

    virtual void CreateForm() = 0;
    void PrintStatistics(std::string AppendString) override {
        std::printf("# Mesh Statistic\nHeight=%1$i, Weight=%2$i%3$s\n", Height, Weight, AppendString.c_str());
    };
};


class CarMesh: public Mesh
{
protected:
    int WheelDiameter{0};
public:
    CarMesh() = default;
    ~CarMesh() override = default;

    void CreateForm() override {
        this->Height = 10;
        this->Weight = 100;
        this->WheelDiameter = 2;
    };
    void PrintStatistics(std::string AppendString) override {
        std::string WheelDiameterStr = ", WheelDiameter=" + std::to_string(WheelDiameter);
        Mesh::PrintStatistics(WheelDiameterStr);
    };
};


class PlaneMesh: public Mesh
{
protected:
    int Wingspan{0};
public:
    PlaneMesh() = default;
    ~PlaneMesh() override = default;

    void CreateForm() override {
        this->Height = 20;
        this->Weight = 1000;
        this->Wingspan = 2;
    };
    void PrintStatistics(std::string AppendString) override {
        std::string WingspanStr = ", Wingspan=" + std::to_string(Wingspan);
        Mesh::PrintStatistics(WingspanStr);
    };
};


/////////////////////////////////////////////////////////////////////////////////////////
//// Coordinate System
/////////////////////////////////////////////////////////////////////////////////////////

// Abstract class CoordinateSystem
class CoordinateSystem: public IStatistics
{
protected:
    int x{0};
    int y{0};
public:
    CoordinateSystem() = default;
    virtual ~CoordinateSystem() = default;

    virtual void CreateSpawnPoint() = 0;
    void PrintStatistics(std::string AppendString) override {
        std::printf("# CoordinateSystem Statistic\nSpawn point is: x=%1$i, y=%2$i%3$s\n", x, y, AppendString.c_str());
    };
};


class CarCoordinateSystem: public CoordinateSystem
{
public:
    CarCoordinateSystem() = default;
    ~CarCoordinateSystem() override = default;

    void CreateSpawnPoint() override {
        this->x = 0;
        this->y  = 101;
    }
};


class PlaneCoordinateSystem: public CoordinateSystem
{
protected:
    int z{0};
public:
    PlaneCoordinateSystem() = default;
    ~PlaneCoordinateSystem() override = default;

    void CreateSpawnPoint() override {
        this->x = 0;
        this->y  = 101;
        this->z = 202;
    }
    void PrintStatistics(std::string AppendString) override {
        std::string ZStr = ", z=" + std::to_string(z);
        CoordinateSystem::PrintStatistics(ZStr);
    }
};

/////////////////////////////////////////////////////////////////////////////////////////
//// Abstract Factory
/////////////////////////////////////////////////////////////////////////////////////////

class ITransport
{
public:
    ITransport() = default;
    virtual ~ITransport() = default;

    virtual Mesh* CreateMesh() = 0;
    virtual CoordinateSystem* CreateCoordinateSystem() = 0;
};

/////////////////////////////////////////////////////////////////////////////////////////
//// Concrete Factory for type of transport
/////////////////////////////////////////////////////////////////////////////////////////

class CarTransport: public ITransport
{
public:
    CarTransport() = default;
    ~CarTransport() override = default;

    Mesh* CreateMesh() override {
        return new CarMesh();
    }

    CoordinateSystem* CreateCoordinateSystem() override {
        return new CarCoordinateSystem();
    }
};


class PlaneTransport: public ITransport
{
public:
    PlaneTransport() = default;
    ~PlaneTransport() override = default;

    Mesh* CreateMesh() override {
        return new PlaneMesh();
    }

    CoordinateSystem* CreateCoordinateSystem() override {
        return new PlaneCoordinateSystem();
    }
};


void CreateObjectFromFactory(ITransport* Transport)
{
    if (Transport)
    {
        Mesh* MyMesh = Transport->CreateMesh();
        MyMesh->CreateForm();
        MyMesh->PrintStatistics("\n");

        CoordinateSystem* MyCoordinateSystem = Transport->CreateCoordinateSystem();
        MyCoordinateSystem->CreateSpawnPoint();
        MyCoordinateSystem->PrintStatistics("\n");

        delete MyMesh;
        delete MyCoordinateSystem;
    }
    else {
        std::printf("Transport is not created!");
    }
}


int main() {
    ITransport* Transport;

    //for class Car
    std::printf("## Instance of class Car\n");
    Transport = dynamic_cast<ITransport*>(new CarTransport());
    CreateObjectFromFactory(Transport);


    //for class Plane
    std::printf("## Instance of class Plane\n");
    Transport = dynamic_cast<ITransport*>(new PlaneTransport());
    CreateObjectFromFactory(Transport);

    delete Transport;

    return 0;
}
